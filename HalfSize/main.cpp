//
//  main.cpp
//  HalfSize
//
//  Created by Алексей Еремин on 26/08/2018.
//  Copyright © 2018 Алексей Еремин. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <assert.h>
#include <vector>
#include <functional>
#include <memory>
#include <string>
#include <cstdio>
#include <type_traits>
#include <array>
#include <iterator>
#include <algorithm>

// utility functions
template <typename... Args>
inline void log(const char* format, Args&&... args)
{
    std::printf(format, std::forward<Args>(args)...);
}

#define assert_msg(exp, msg)            \
    if (!(exp))                         \
        log("Assertion fail: %s\n", msg); \
    assert(exp);

#define assert_fail(msg) assert_msg(false, msg)

using RGBAColor = std::array<unsigned char, 4>;
using RGBColor = std::array<unsigned char, 3>;

struct TgaHeader {
   char  idlength;
   char  colourmaptype;
   char  datatypecode;
   short int colourmaporigin;
   short int colourmaplength;
   char  colourmapdepth;
   short int x_origin;
   short int y_origin;
   short width;
   short height;
   char  bitsperpixel;
   char  imagedescriptor;
} __attribute__((packed));

using BinaryData = std::vector<unsigned char>;

struct Size
{
   short width;
   short height;
   
   unsigned int resolution() const { return width * height; }
   
   friend Size operator / (const Size& size, int div)
   {
      Size result = size;
      result.width /= 2;
      result.height /= 2;
      return result;
   }
};

class ImageFormat
{
public:
   enum class Type {
      RGB24,
      RGBA32,
      RGBA16,
      UNDEF
   };
   
   ImageFormat() = default;
   
   ImageFormat(const BinaryData& data)
   {
      parse(data);
   }
   
   TgaHeader& getHeader()
   {
      commitChanges();
      return m_rawHeader;
   }
   
   Type getType() const { return m_type; }
   bool isSupported() const { return m_isSupported; }
   char getPixelSize() const { return m_rawHeader.bitsperpixel >> 3; }
   const Size& getSize() const { return m_size; }
   int getImageSizeInBytes() const { return getNumTexels() * getPixelSize(); }
   int getNumTexels() const { return m_size.resolution(); }
   int getImageDataOffset() const { return sizeof(TgaHeader); }
   
   void setSize(const Size& value)
   {
      assert(value.width > 0 && value.height > 0);
      m_size = value;
      changeFormat();
   }
   
private:
   void commitChanges()
   {
      if (!m_isDirty) {
         return;
      }
      
      m_rawHeader.width = m_size.width;
      m_rawHeader.height = m_size.height;
      
      m_isDirty = false;
   }
   
   void changeFormat()
   {
      m_isDirty = true;
   }
   
   void parse(const BinaryData& data)
   {
      assert_msg(data.size() >= sizeof(TgaHeader), "wrong file format");
      memcpy(&m_rawHeader, data.data(), sizeof(TgaHeader));
      
      m_size = {m_rawHeader.width, m_rawHeader.height};
      
      switch (m_rawHeader.bitsperpixel) {
         case 16 : m_type = Type::RGBA16; break;
         case 24 : m_type = Type::RGB24;  break;
         case 32 : m_type = Type::RGBA32; break;
         default: m_type = Type::UNDEF;
      }
      
      m_isSupported = m_rawHeader.datatypecode == 2 // Unmapped RGB
                     && (m_type == Type::RGB24 || m_type == Type::RGBA32);
      
      m_isDirty = false;
   }
   
   bool m_isDirty;
   
   bool m_isSupported;
   Size m_size;
   Type m_type;
   
   TgaHeader m_rawHeader;
};

class File {
public:
   File() = default;
   
   virtual void read(const std::string& path)
   {
      auto file = open(path, true);
      
      if (!file) {
         log("Couldn't open file: %s\n", path.c_str());
         return;
      }
      
      fseek(file.get(), 0, SEEK_END);
      size_t fileSize = ftell(file.get());
      rewind(file.get());
      
      m_data.resize(fileSize);
      fread(m_data.data(), fileSize, 1, file.get());
   }
   
   virtual void store(const std::string& path)
   {
      auto file = open(path, false);
      
      if (!file) {
         log("Couldn't open file: %s\n", path.c_str());
         return;
      }
      
      fwrite(m_data.data(), m_data.size(), 1, file.get());
      
      if (ferror(file.get()))
         perror("Error:");
   }
   
   void setData(BinaryData data)
   {
      m_data = std::move(data);
   }
   
   const BinaryData& getData() const
   {
      return m_data;
   }
   
   BinaryData& getData()
   {
      return m_data;
   }
   
   
private:
   using FilePtr = std::unique_ptr<FILE, void(*)(FILE*)>;
   FilePtr open(const std::string& path, bool read)
   {
      return FilePtr(
                     fopen(path.c_str(), read ? "rb" : "wb"),
                     [](FILE* ptr) {
                        if (ptr) ::fclose(ptr);
                        
                     }
                     );
   }

   BinaryData m_data;
};

class ImageFile : public File {
public:
   ImageFile() = default;
   
   ImageFile(ImageFormat format, BinaryData data)
   {
      m_format = std::move(format);
      m_texels = std::move(data);
   }
   
   void read(const std::string& path) override
   {
      File::read(path);
      
      m_format = ImageFormat(getData());
   }
   
   void store(const std::string& path) override
   {
      const auto headerSize = sizeof(m_format.getHeader());
      const auto imageDataSize = m_texels.size();
      getData().resize(headerSize + imageDataSize);
      memcpy(getData().data(), &m_format.getHeader(), headerSize);
      memcpy(getData().data() + headerSize, m_texels.data(), imageDataSize);
      
      File::store(path);
   }
   
   const ImageFormat& getFormat() const { return m_format; }

private:
   BinaryData  m_texels;
   ImageFormat m_format;
};

class HalfResConverter
{
public:
   static const int NUM_TEXELS_PER_CHUNK = 4;
   
   HalfResConverter(const ImageFile& image) : m_source{image} {}
   
   std::tuple<ImageFile, bool> convert()
   {
      ImageFile result;
      
      const ImageFormat::Type type = m_source.getFormat().getType();
      
      assert_msg(m_source.getFormat().isSupported(), "Unsupported image file");
      
      if (type == ImageFormat::Type::RGBA32) {
         log("Process RGBA32 image\n");
         return process<RGBAColor, 4>();
      }
         
      if (type == ImageFormat::Type::RGB24) {
         log("Process RGB24 image\n");
         return process<RGBColor, 3>();
      }
      
      assert_fail("Unsupported image format");
      
      return {result, false};
   }
   
private:
   template <typename ColorType>
   struct PacketData {
      const std::vector<ColorType>* source;
      std::vector<ColorType>* result;
      
      int numChunksToProcess;
      int startChunk;
   };
   
   template <typename ColorType, int N>
   std::tuple<ImageFile, bool> process()
   {
      const auto sourceTexels = pack<ColorType, N>();
      std::vector<ColorType> resizedTexels(sourceTexels.size() / 4);
      
      const int sizePerPacket = 64 * N; // fit result writes into N cache-lines to avoid false sharing
      const int numPackets = 1 + ((resizedTexels.size() - 1) / sizePerPacket); // round up
      for (int i = 0; i < numPackets; ++i) {
         PacketData<ColorType> packet = { &sourceTexels, &resizedTexels, sizePerPacket, sizePerPacket * i};
         resize<ColorType, N>(packet);
      }
      
      ImageFormat format = m_source.getFormat();
      const Size size = format.getSize() / 2;
      format.setSize(size);
      
      BinaryData data(format.getImageSizeInBytes());
      memcpy(data.data(), resizedTexels.data(), format.getImageSizeInBytes());
      
      return {ImageFile{format, data}, true};
   }
   
   /**
    // pack texel data into z-order chunks
    // 0 - 1   4 - 5
    //   /   /   /    ...
    // 2 - 3   6 - 7
    **/
   template <typename ColorType, int N>
   std::vector<ColorType> pack()
   {
      log("Pack image data\n");
      
      const ImageFormat& format = m_source.getFormat();
      const int imageWidth = format.getSize().width;
      const int imageHeight = format.getSize().width;
      const int numChunksPerRow = imageWidth / 2;
      const int numChunksPerColumn = imageHeight / 2;
      
      std::vector<ColorType> result(numChunksPerRow * numChunksPerColumn * 4);
      
      const auto start = format.getImageDataOffset();
      
      for (int y = 0; y < numChunksPerColumn; ++y) {
         for (int x = 0; x < numChunksPerRow; ++x) {
            const int texelStartIdx = (x + y * numChunksPerRow) * NUM_TEXELS_PER_CHUNK;
            
            const int readPosition01 = start + format.getPixelSize() * (x * 2 + y * 2 * imageWidth);
            memcpy(&result[texelStartIdx], m_source.getData().data() + readPosition01, N * 2);
            
            const int readPosition23 = start + format.getPixelSize() * (x * 2 + (y + 1) * 2 * imageWidth);
            memcpy(&result[texelStartIdx + 2], m_source.getData().data() + readPosition23, N * 2);
         }
      }
      
      return result;
   }
   
   template <typename ColorType, int N>
   void resize(PacketData<ColorType> input)
   {
      log("Resizing the image...\n");
      
      const std::vector<ColorType>& source = *input.source;
      std::vector<ColorType>& result = *input.result;
      
      const int writeStart = input.startChunk;
      const int writeEnd = std::min(writeStart + input.numChunksToProcess, (int)result.size());
      assert(writeEnd <= result.size());
      for (int i = writeStart; i < writeEnd; ++i) {
         std::array<float, N> color = {};
         for (int j = 0; j < NUM_TEXELS_PER_CHUNK; ++j) {
            for (int k = 0; k < N; ++k) {
               color[k] += source[i * NUM_TEXELS_PER_CHUNK + j][k];
            }
         }
         
         for (int k = 0; k < N; ++k) {
            result[i][k] = color[k] * 0.25;
         }
      }
   }
   
   const ImageFile& m_source;
};

void run_test()
{
   
}

int main(int argc, const char * argv[]) {
   assert_msg(sizeof(TgaHeader) == 18, "Tga header must be tightly packed");
   
   ImageFile image;
   image.read("test_rgb.tga");
   
   bool success = false;
   ImageFile result;
   std::tie(result, success) = HalfResConverter(image).convert();
   
   assert_msg(success, "Failed to convert an image");
   
   if (success) {
      result.store("test_simple_.tga");
   }
   
   return 0;
}
